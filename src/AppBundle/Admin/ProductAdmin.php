<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Product;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class ProductAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('fullName')
            ->add('image')
            ->add('description')
            ->add('brand')
            ->add('model')
            ->add('price')
            ->add('discount')
            ->add('views')

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('fullName')
            ->add('image')
            ->add('description')
            ->add('brand')
            ->add('model')
            ->add('price')
            ->add('discount')
            ->add('views')
            ->add('owner')
            ->add('showOnHomepage')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('fullName')
            ->add('image')
            ->add('description')
            ->add('categoryId')
            ->add('brand')
            ->add('model')
            ->add('price')
            ->add('discount')
            ->add('views')
            ->add('showOnHomepage')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('fullName')
            ->add('image')
            ->add('description')
            ->add('categoryId')
            ->add('brand')
            ->add('model')
            ->add('price')
            ->add('discount')
            ->add('views')
            ->add('owner')
            ->add('showOnHomepage')
        ;
    }
    /**
     * @param Product $object
     */
    public function preUpdate($object)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $object->setOwner($user);
        parent::preUpdate($object);
    }

    /**
     * @param Product $object
     */
    public function prePersist($object)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $object->setOwner($user);
        parent::prePersist($object);
    }


}
