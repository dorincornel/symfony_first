<?php

namespace AppBundle\Services;

use AppBundle\Entity\Product;
use AppBundle\Entity\Reviews;
use Doctrine\Bundle\DoctrineBundle\Registry;


class ReviewService
{
    /** @var  Registry */
    private $doctrine;

    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param Registry $doctrine
     * @return ReviewService
     */
    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
        return $this;
    }

    public function getReviews($id)
    {
      return $this->getDoctrine()->getManager()->getRepository(Reviews::class)->findBy(['productId'=>$id]);
    }

    public function ratingFinal($id)
    {
        $totalRating = 0;
        $reviews = $this->getReviews($id);
        foreach($reviews as $value):
            $totalRating += $value->getRating();
        endforeach;
        if(count($reviews) != 0) {
            $finalRating = $totalRating / count($reviews);
        }else{
            $finalRating = 0; }
        return $finalRating;
    }

    public function totalReviews($id)
    {
        $reviews = $this->getReviews($id);
        return count($reviews);
    }


}