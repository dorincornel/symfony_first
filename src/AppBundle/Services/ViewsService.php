<?php

namespace AppBundle\Services;


use AppBundle\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Registry;



class ViewsService
{

   /** @var  Registry */
   private $doctrine;

     /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param Registry $doctrine
     * @return ViewsService
     */
    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
        return $this;
    }

    public function addView($id)
    {
        $rep = $this->getDoctrine()->getManager()->getRepository(Product::class);
        $view = $rep->find($id)->getViews();
        if ($view == null) {
            $view = 0;
        }
        $view = $view + 1;
        $prod = $rep->find($id);
        $prod->setViews($view);

        $this->getDoctrine()->getManager()->persist($prod);
        $this->getDoctrine()->getManager()->flush();
    }


}