<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use AppBundle\Entity\Reviews;
use AppBundle\Entity\Users;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */

    public function contactAction(Request $request)
    {
        return $this->render('default/contact.html.twig', ['name' => $request->get('name')]);
    }

    public function productAction(Request $request)
    {
        $product = $this->getDoctrine()->getManager()->getRepository(Product::class)->find($request->get('id'));
        return $this->render('default/product.html.twig', ['product' => $product]);
    }

    public function categoriesAction(Request $request)
    {
        $ItemsPerRow = 6;
        $repository = $this->getDoctrine()
            ->getRepository(Product::class);
        $query = $repository->createQueryBuilder('p')
            ->where('p.categoryId = :categ')
            ->setParameter('categ', $request->get('name'))
            ->orderBy('p.price - p.discount', 'ASC')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12 /*limit per page*/
        );


        $filter['parentId'] = '0';
        $menFilter = $this->getDoctrine()->getManager()->getRepository(Category::class)->findBy($filter);
        return $this->render('default/categories.php.twig', ['menFilter' => $menFilter , 'ItemsPerRow' => $ItemsPerRow ,
            'pagination' => $pagination]);
        
        }

    public function indexAction(Request $request)
    {
        $ItemsPerRow = 6;
        $repository = $this->getDoctrine()
            ->getRepository(Product::class);
        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.discount', 'DESC')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12 /*limit per page*/
        );

        $filter['parentId'] = '0';
        $menFilter = $this->getDoctrine()->getManager()->getRepository(Category::class)->findBy($filter);
        return $this->render('default/index.php.twig', ['menFilter' => $menFilter , 'ItemsPerRow' => $ItemsPerRow ,
            'pagination' => $pagination]);

    }

    public function searchAction(Request $request)
       {
           $ItemsPerRow = 6;
           $repository = $this->getDoctrine()
               ->getRepository(Product::class);
           $query = $repository->createQueryBuilder('p')
               ->where('p.fullName LIKE :crt')
               ->orWhere('p.description LIKE :crt')
               ->setParameter('crt', '%'.$request->get('criteriu').'%')
               ->orderBy('p.price - p.discount', 'ASC')
               ->getQuery();

           $paginator = $this->get('knp_paginator');
           $pagination = $paginator->paginate(
               $query, /* query NOT result */
               $request->query->getInt('page', 1), /*page number*/
               12 /*limit per page*/
           );

           $filter['parentId'] = '0';
           $menFilter = $this->getDoctrine()->getManager()->getRepository(Category::class)->findBy($filter);
           return $this->render('default/index.php.twig', ['menFilter' => $menFilter , 'ItemsPerRow' => $ItemsPerRow ,
               'pagination' => $pagination]);
       }

    /**
     * @Route("/description/{name}", name="description")
     */
    public function descriptionAction(Request $request)
    {
    $id = $request->get('name');
    $repProd = $this->getDoctrine()->getManager()->getRepository(Product::class);
    $produs = $repProd->find($id);

    $this->get('views.service')->addView($id);

    $repImg = $this->getDoctrine()->getManager()->getRepository(Image::class);
    $selImages = $repImg->findby(['productId'=>$id]);

    $reviews = $this->get('review.service')->getReviews($id);
    $finalRating = $this->get('review.service')->ratingFinal($id);
    $totalRev = $this->get('review.service')->totalReviews($id);

    $simFilter['categoryId'] = $produs->getCategoryId();
    $produseSimilare = $repProd->findBy($simFilter, null, 6);

    $filter['parentId'] = '0';
    $menFilter = $this->getDoctrine()->getManager()->getRepository(Category::class)->findBy($filter);

    return $this->render('default/description.php.twig' ,
           ['produs' => $produs , 'selImages' => $selImages , 'menFilter' => $menFilter ,
            'reviewRating' => $finalRating , 'reviewTotal' => $totalRev ,
            'produseSimilare' => $produseSimilare , 'reviews' => $reviews]);
   }
    /**
     * @Route("/review/{id}", name="review")
     */
    public function reviewAction(Request $request)
    {
        $id = $request->get('id');
        $repProd = $this->getDoctrine()->getManager()->getRepository(Product::class);
        $produs = $repProd->find($id);
        $simFilter['categoryId'] = $produs->getCategoryId();
        $produseSimilare = $repProd->findBy($simFilter, null, 6);

        $filter['parentId'] = '0';
        $menFilter = $this->getDoctrine()->getManager()->getRepository(Category::class)->findBy($filter);

        $review = new Reviews();
        $user = 'Anonim';
        $data = date('Y-m-d');
        $revDate = \DateTime::createFromFormat("Y-m-d", $data);
        $review->setUsername($user);
        $review->setProductId($id);
        $review->setAddDate($revDate);
        $review->setMessage('Descrie experienta ta cu produsul');

        $form = $this->createFormBuilder($review)
            ->add('Rating', ChoiceType::class, [
            'choices' => [
                'star1' => '1', 'star2' => '2', 'star3' => '3', 'star4' => '4', 'star5' =>'5'],
                'expanded' => 'true'])
            ->add('message', TextareaType::class)
            ->add('title', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Adauga review'])
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $review = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($review);
                $em->flush();

                return $this->redirectToRoute('description',['name'=>$id]);
            }

        return $this->render('default/review.php.twig', [
            'form' => $form->createView(), 'produs' => $produs,
            'menFilter' => $menFilter, 'produseSimilare' => $produseSimilare
        ]);

    }

    public function conectAction()
    {
       return $this->render('default/cont.php.twig');
    }


}


