<br><br>
<div class="row promo">

    {% set i=0 %}
    {% set par = 12/ItemsPerRow %}
    {% for line in pagination %}
    <div class="col-sm-{{ par }} card-group text-center">
        <div class="card">
            <span class="test">{{ line.discount }} RON </span>
            <a href="{{ url('description', {'name':line.id}) }}"><img style="width:100%" class="card-img-top" src="{{ asset('images/'~ line.image) }}"></a>
            <div class="card-body">
                <a id="list" class="card-title" href="{{ url('description', {'name':line.id}) }}">{{ line.name }}</a>
            </div>
            <div class="card-footer deleteRowSpaces">
                <p class="card-text"><del>{{ line.price }} RON </del></p>
                <p class="card-pret"><b>{{ line.price - line.discount }} RON </b></p>
                <a class="cos btn" href="add_cos.php?productId={{ line.getID() }}&quantity=1"><i class='fas fa-shopping-cart'></i> &nbsp Adauga &nbsp </a>
            </div>
        </div>
    </div>
       {% set i=i+1 %}
       {% if i%ItemsPerRow  == 0 %}
           </div>
           <div class="row promo">
       {% endif %}
    {% endfor %}

 </div><br>
<div class="row">
    <div class="col-sm-10 paginator">
        {{ knp_pagination_render(pagination) }}
    </div>
    <div class="col-sm-2 paginator">
        Total: {{ pagination.getTotalItemCount }}
    </div>
</div><br>








