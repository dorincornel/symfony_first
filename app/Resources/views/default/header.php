<div class="row hd1">
    <div class="col-sm-3 my-auto">
        <a href="{{ url('index') }}"><img src="{{ asset('images/logo.jpg') }}" width="120"></a>
    </div>
    <div class="col-sm-6 my-auto">
        <form class="form-inline" action="{{ url('search') }}" method="get">
            <div class="input-group">
                <input class="form-control my-0 py-1 red-border" type="text" placeholder="Cauta produs" name="criteriu" aria-label="Search">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Cauta</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-3 my-auto">
        <div class="right">
            <a class="hed" href="{{ url('admin') }}">&nbsp&nbsp&nbsp Admin &nbsp&nbsp</a>
            {% if app.session.user is defined %}
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $_SESSION['user']; ?></button>
                    <ul class="dropdown-menu">
                         Salut {{ app.session.user }}
                        <li><a href="">Contul meu</a></li>
                        <li><a href="">Setari</a></li>
                        <li><a href="">Comenzile mele</a></li>
                        <li><a href="logout.php">Deconectare</a></li>
                    </ul>
                    <a class="hed" href="list_cos.php"><i class='fas fa-shopping-cart'></i> Cosul meu </a>
                </div>
            {% else %}
            <a class="hed" href="{{ url('conectare') }}">Cont &nbsp&nbsp&nbsp </a>
            <a class="hed" href="list_cos.php"><i class='fas fa-shopping-cart'></i> Cosul meu </a>
            {% endif %}
        </div>
    </div>
</div>

